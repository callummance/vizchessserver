package main

import (
    "fmt"
    "strings"
)

type Move struct {
    Fromx int
    Fromy int
    Tox int
    Toy int
    Promote byte
}

func isWhite(piece byte) bool {
    return piece >= 65 && piece <= 90
}

func isBlack (piece byte) bool {
    return piece >= 90 && piece <= 122
}

func isOnBoard (loc int) bool {
    return loc >= 1 && loc <= 8
}

func NewBoard() [10][10]byte {
    res := [10][10]byte{{' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '},
                        {' ', 'R','N','B','Q','K','B','N','R', ' '},
                        {' ', 'P','P','P','P','P','P','P','P', ' '},
                        {' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '},
                        {' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '},
                        {' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '},
                        {' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '},
                        {' ', 'p','p','p','p','p','p','p','p', ' '},
                        {' ', 'r','n','b','q','k','b','n','r', ' '},
                        {' ', ' ',' ',' ',' ',' ',' ',' ',' ', ' '}}
    return res
}

func PrintBoard(brd [10][10]byte) {
    fmt.Print("  |-------------------------------|\n")
    for rank:=8; rank>=1; rank-- {
        fmt.Printf("%d ", rank)
        fmt.Print("|")
        for file:=1; file<=8; file++ {
            fmt.Printf(" %s |", string(brd[rank][file]))
        }
        fmt.Print("\n  |-------------------------------|\n")
    }
    fmt.Println("    A   B   C   D   E   F   G   H")
}

func(mov Move) Encode() string {
    if mov.Promote == 'n' || mov.Promote == 0 {
        return fmt.Sprintf("%d %d - %d %d\n", mov.Fromy, mov.Fromx, mov.Toy, mov.Tox)
    } else {
        return fmt.Sprintf("%d %d - %d %d & %s\n", mov.Fromy, mov.Fromx, mov.Toy, mov.Tox, string(mov.Promote))
    }

}

func Decode(str string) Move {
    mov := new(Move)
    toks, err := fmt.Sscanf(str, "%d %d - %d %d", &mov.Fromy, &mov.Fromx, &mov.Toy, &mov.Tox)
    fmt.Print(str)
    if err != nil {panic(fmt.Sprintf("Malformed Move. PANNNIIIIIC!, %s, %d", err, toks))}


    return *mov
}

func possibleMove(mov Move, pMov Move) bool {
    if mov.Fromx != pMov.Fromx || mov.Fromy != pMov.Fromy {return false}
    if mov.Tox != pMov.Tox || mov.Toy != pMov.Toy {return false}
    if pMov.Promote == 'y' {return true}
    if mov.Promote == 0 {return true}
    return false
}

func CanPromote(mov Move, brd [10][10]byte) bool {
    if brd[mov.Fromx][mov.Fromy] == 'P' && mov.Tox == 8 {
        return true
    }
    if brd[mov.Fromx][mov.Fromy] == 'p' && mov.Tox == 1 {
        return true
    }
    return false
}



func IsValidMove(brd [10][10]byte, mov Move, isWhiteMove bool) bool {
    if !isOnBoard(mov.Fromx) || !isOnBoard(mov.Fromy) || !isOnBoard(mov.Tox) || !isOnBoard(mov.Tox){
        return false
    }
    if isWhite(brd[mov.Fromx][mov.Fromy]) != isWhiteMove {
        return false
    }
    if isWhite(brd[mov.Fromx][mov.Fromy]) {
        whiteMoves := WPLMoveGen(brd)
        for _,move := range whiteMoves {
            if possibleMove(mov, move) && WIsLegal(MakeMove(brd, mov)) {
                return true
            }
        }
        return false

    } else if isBlack(brd[mov.Fromx][mov.Fromy]) {
        blackMoves := BPLMoveGen(brd)
        for _,move:= range blackMoves {
            if possibleMove(mov, move) && BIsLegal(MakeMove(brd, mov)) {
                return true
            }
        }
        return false
    } else {
        return false
    }
}

func MakeMove(brd [10][10]byte, mov Move) [10][10]byte {
    pieceMoving := brd[mov.Fromx][mov.Fromy]
    if mov.Promote != 'n' && mov.Promote != 0 {
        if pieceMoving == 'p' {
            pieceMoving = byte(strings.ToLower(string(mov.Promote))[0])
        } else {
            pieceMoving = byte(strings.ToUpper(string(mov.Promote))[0])
        }
    }
    brd[mov.Fromx][mov.Fromy] = ' '
    //En Passant
    if (pieceMoving == 'p' || pieceMoving == 'P') && brd[mov.Tox][mov.Toy] == ' ' {
        brd[mov.Fromx][mov.Toy] = ' '
    }
    brd[mov.Tox][mov.Toy] = pieceMoving
    return brd
}

func BIsLegal(brd [10][10]byte) bool {
    whiteMoves := WPLMoveGen(brd)
    var kingX int
    var kingY int

    for i,rank := range brd {
        for j, square := range rank {
            if square == 'k' {
                kingX = i
                kingY = j
            }
        }
    }

    if kingX == 0 || kingY == 0 {
        fmt.Print("No king? WTF?!?!?")
    }

    for _, move := range whiteMoves {
        if move.Tox == kingX && move.Toy == kingY {
            return false
        }
    }
    return true
}

func WIsLegal(brd [10][10]byte) bool {
    blackMoves := BPLMoveGen(brd)
    var kingX int
    var kingY int

    for i,rank := range brd {
        for j, square := range rank {
            if square == 'K' {
                kingX = i
                kingY = j
            }
        }
    }

    if kingX == 0 || kingY == 0 {
        fmt.Print("No king? WTF?!?!?")
    }

    for _, move := range blackMoves {
        if move.Tox == kingX && move.Toy == kingY {
            return false
        }
    }
    return true
}

func BPLMoveGen(board [10][10]byte) []Move {
    moveList := make([]Move, 0)
    for i, rank := range board {
        for j, square := range rank {
            switch {
            case square == 'p':
                if board[i-1][j] == ' ' {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-1, Toy: j, Promote: 'n'})
                }
                if board[i-2][j] == ' ' && i == 7 {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-2, Toy: j, Promote: 'n'})
                }
                if isWhite(board[i-1][j-1]) {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-1, Toy: j-1, Promote: 'n'})
                }
                if isWhite(board[i-1][j+1]) {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-1, Toy: j+1, Promote: 'n'})
                }
                //En Passant
                if board[i][j-1] == 'P' && i == 4{
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-1, Toy: j-1, Promote: 'n'})
                }
                if board[i][j+1] == 'P' && i == 4{
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i-1, Toy: j+1, Promote: 'n'})
                }
            case square == 'r':
                var di int
                for {
                    di += 1
                    tgt := i+di
                    if tgt > 8 {break}
                    if isBlack(board[tgt][j]) {break}
                    if isWhite(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                di = 0
                for {
                    di -= 1
                    tgt := i+di
                    if tgt < 1 {break}
                    if isBlack(board[tgt][j]) {break}
                    if isWhite(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                var dj int
                for {
                    dj += 1
                    tgt := j+dj
                    if tgt > 8 {break}
                    if isBlack(board[i][tgt]) {break}
                    if isWhite(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                dj = 0
                for {
                    di -= 1
                    tgt := j+dj
                    if tgt < 1 {break}
                    if isBlack(board[i][tgt]) {break}
                    if isWhite(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }

            case square == 'n':
                var tgti int
                var tgtj int

                tgti = i + 2
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 2
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 2
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 2
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j + 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

            case square == 'b':
                var diffi int
                var diffj int
                var di int
                var dj int

                diffi = 0
                diffj = 0
                di = 1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = 1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            case square == 'q':
                var di int
                for {
                    di += 1
                    tgt := i+di
                    if tgt > 8 {break}
                    if isBlack(board[tgt][j]) {break}
                    if isWhite(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                di = 0
                for {
                    di -= 1
                    tgt := i+di
                    if tgt < 1 {break}
                    if isBlack(board[tgt][j]) {break}
                    if isWhite(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                var dj int
                for {
                    dj += 1
                    tgt := j+dj
                    if tgt > 8 {break}
                    if isBlack(board[i][tgt]) {break}
                    if isWhite(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                dj = 0
                for {
                    di -= 1
                    tgt := j+dj
                    if tgt < 1 {break}
                    if isBlack(board[i][tgt]) {break}
                    if isWhite(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                var diffi int
                var diffj int

                diffi = 0
                diffj = 0
                di = 1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = 1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isBlack(board[tgti][tgtj]) {break}
                    if isWhite(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            case square == 'k':
                var tgti int
                var tgtj int

                tgti = i + 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isBlack(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            }
        }
    }
    return moveList
}


func WPLMoveGen(board [10][10]byte) []Move {
    moveList := make([]Move, 0)
    for i, rank := range board {
        for j, square := range rank {
           switch {
            case square == 'P':
                if board[i+1][j] == ' ' {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+1, Toy: j, Promote: 'n'})
                }
                if board[i+2][j] == ' ' && i == 2 {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+2, Toy: j, Promote: 'n'})
                }
                if isBlack(board[i+1][j-1]) {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+1, Toy: j-1, Promote: 'n'})
                }
                if isBlack(board[i+1][j+1]) {
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+1, Toy: j+1, Promote: 'n'})
                }
                //En Passant
                if board[i][j-1] == 'p' && i == 5{
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+1, Toy: j-1, Promote: 'n'})
                }
                if board[i][j+1] == 'p' && i == 5{
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i+1, Toy: j+1, Promote: 'n'})
                }
            case square == 'R':
                var di int
                for {
                    di += 1
                    tgt := i+di
                    if tgt > 8 {break}
                    if isWhite(board[tgt][j]) {break}
                    if isBlack(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                di = 0
                for {
                    di -= 1
                    tgt := i+di
                    if tgt < 1 {break}
                    if isWhite(board[tgt][j]) {break}
                    if isBlack(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                var dj int
                for {
                    dj += 1
                    tgt := j+dj
                    if tgt > 8 {break}
                    if isWhite(board[i][tgt]) {break}
                    if isBlack(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                dj = 0
                for {
                    di -= 1
                    tgt := j+dj
                    if tgt < 1 {break}
                    if isWhite(board[i][tgt]) {break}
                    if isBlack(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }

            case square == 'N':
                var tgti int
                var tgtj int

                tgti = i + 2
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 2
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 2
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 2
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j + 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 2
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

            case square == 'B':
                var diffi int
                var diffj int
                var di int
                var dj int

                diffi = 0
                diffj = 0
                di = 1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = 1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            case square == 'Q':
                var di int
                for {
                    di += 1
                    tgt := i+di
                    if tgt > 8 {break}
                    if isWhite(board[tgt][j]) {break}
                    if isBlack(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                di = 0
                for {
                    di -= 1
                    tgt := i+di
                    if tgt < 1 {break}
                    if isWhite(board[tgt][j]) {break}
                    if isBlack(board[tgt][j]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgt, Toy: j, Promote: 'n'})
                }
                var dj int
                for {
                    dj += 1
                    tgt := j+dj
                    if tgt > 8 {break}
                    if isWhite(board[i][tgt]) {break}
                    if isBlack(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                dj = 0
                for {
                    di -= 1
                    tgt := j+dj
                    if tgt < 1 {break}
                    if isWhite(board[i][tgt]) {break}
                    if isBlack(board[i][tgt]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: i, Toy: tgt, Promote: 'n'})
                }
                var diffi int
                var diffj int

                diffi = 0
                diffj = 0
                di = 1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = 1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = -1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }

                diffi = 0
                diffj = 0
                di = 1
                dj = -1
                for {
                    diffi -= di
                    diffj -= dj
                    tgti := i+diffi
                    tgtj := j+diffj
                    if (!isOnBoard(tgti) || !isOnBoard(tgtj)) {break}
                    if isWhite(board[tgti][tgtj]) {break}
                    if isBlack(board[tgti][tgtj]) {
                        moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                        break
                    }
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            case square == 'K':
                var tgti int
                var tgtj int

                tgti = i + 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j + 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i + 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
                tgti = i - 1
                tgtj = j - 1
                if isOnBoard(tgti) && isOnBoard(tgtj) && !isWhite(board[tgti][tgtj]){
                    moveList = append(moveList, Move{Fromx: i, Fromy: j, Tox: tgti, Toy: tgtj, Promote: 'n'})
                }
            }
        }
    }
    return moveList
}
