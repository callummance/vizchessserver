package main

import (
    "fmt"
    "net"
    "bufio"
)

func main() {
    port := ":12345"
    fmt.Printf("Starting server on port %v\n", port)
    ln, err := net.Listen("tcp", port)
    if err != nil {
        fmt.Printf("Server failed to start (lol)")
    }

    fmt.Println("Waiting for player 1 to connect...")
    conn1, err := ln.Accept()
    if err != nil {
        fmt.Printf("Player 1 broke!")
    }

    fmt.Println("Waiting for player 2 to connect...")
    conn2, err := ln.Accept()
    if err != nil {
        fmt.Printf("Player 2 broke!")
    }

    board := NewBoard()
    PrintBoard(board)

    for {
        board = makeMove(board, &conn1, &conn2, true)
        board = makeMove(board, &conn2, &conn1, false)
    }
}

func makeMove(brd [10][10]byte, conn1 *net.Conn, conn2 *net.Conn, isWhiteMove bool) [10][10]byte {
    message, _ := bufio.NewReader(*conn1).ReadString('\n')
    mov := Decode(string(message))
    for !IsValidMove(brd, mov, isWhiteMove) {
        (*conn1).Write([]byte("no\n"))
        fmt.Printf("Move %v was rejected.\n", mov)
        message, _ = bufio.NewReader(*conn1).ReadString('\n')
        mov = Decode(string(message))
    }
    if CanPromote(mov, brd) {
        (*conn1).Write([]byte("promote?"))
        message, _ = bufio.NewReader(*conn1).ReadString('\n')
        for message != "q\n" && message != "Q\n" && message != "k\n" && message != "K\n" && message != "r\n" && message != "R\n" && message != "b\n" && message != "B\n" {
            (*conn1).Write([]byte("promote?\n"))
            message, _ = bufio.NewReader(*conn1).ReadString('\n')
            (*conn1).Write([]byte(fmt.Sprintf("Promotion rejected: %s", message)))
        }
        mov.Promote = byte(message[0])
    }
    (*conn1).Write([]byte("yes\n"))
    newBrd := MakeMove(brd, mov)
    (*conn1).Write([]byte(string(mov.Encode())))
    (*conn2).Write([]byte(string(mov.Encode())))

    PrintBoard(newBrd)
    fmt.Print("\n\n")

    return newBrd
}
